package hotel.service;

import static org.junit.jupiter.api.Assertions.*;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import hotel.credit.CreditCard;
import hotel.entities.*;
import hotel.service.RecordServiceCTL.State;

import org.junit.jupiter.api.Test;
@ExtendWith(MockitoExtension.class)
class RecordServiceCTLTest {
@Mock RecordServiceCTL recordServiceCTL;
@Mock Hotel hotel;

 enum State {SERVICE};
ServiceType serviceType;
double cost;
@Mock Guest guest;	
@Mock CreditCard card;
@Mock Booking confirmationNumber;
SimpleDateFormat format=new SimpleDateFormat("11-12-2001");
Date arrivalDate;
int stayLength=2;
int numberOfOccupants=5;
Booking booking;
public Map<Long, Booking> bookingsByConfirmationNumber;
public Map<Integer, Booking> activeBookingsByRoomId;
@InjectMocks Room room=new Room(101,RoomType.SINGLE);
@BeforeEach
void setUp() throws Exception {
	
	hotel=new Hotel();
	arrivalDate=format.parse("11-12-2001");	
	bookingsByConfirmationNumber = new HashMap<>();
	activeBookingsByRoomId= new HashMap<>();
	hotel=new Hotel();
	booking =new Booking(guest,room,arrivalDate,stayLength,numberOfOccupants,card);

}
/* Added By Rajashekar Boreddy*/
/* Tested By Rajashekar Boreddy*/
@Test
	final void testServiceDetailsEntered() {
		recordServiceCTL =new RecordServiceCTL(hotel);
		hotel.book(room,guest,arrivalDate,stayLength,numberOfOccupants,card);
		hotel.checkin(101970101);
		recordServiceCTL.roomNumberEntered(101);
		
		recordServiceCTL.serviceDetailsEntered(serviceType,100.02);
	}
/* Added By Rajashekar Boreddy*/
/* Tested By Rajashekar Boreddy*/
	@Test
	final void testServiceDetailsEntered_ShouldThrowException() {
		//arrange
		recordServiceCTL =new RecordServiceCTL(hotel);
		 recordServiceCTL.cancel();
		//act
		Executable e = () -> recordServiceCTL.serviceDetailsEntered(serviceType,cost);
		Throwable t = assertThrows(RuntimeException.class,e);
		
		//assert
		assertEquals("RecordServiceCTL: Service is in incorrect state",t.getMessage());		
	}


}
