package hotel.booking;

import hotel.entities.Booking;
import hotel.entities.Guest;
import hotel.entities.Hotel;
import hotel.entities.Room;
import hotel.entities.RoomType;
import hotel.service.RecordServiceCTL;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;



import org.junit.jupiter.api.Test;
import hotel.credit.*;

@ExtendWith(MockitoExtension.class)


class BookingCTLTest {
CreditCardType type;
SimpleDateFormat format=new SimpleDateFormat("11-12-2100");
@Mock BookingCTL bookingCTL;
@Mock Hotel hotel;
//@Mock int maxOccupancy=10;
int number;
RoomType selectedRoomType;
int ccv; 
@Mock Guest guest;	
@Mock CreditCard card;
@Mock Booking confirmationNumber;
Date arrivalDate;
Date arrivalDate1;
int stayLength=2;
int numberOfOccupants=2;
Booking booking;
public Map<Long, Booking> bookingsByConfirmationNumber;
public Map<Integer, Booking> activeBookingsByRoomId;
enum State {OCCUPIED,READY};
State state;
@Mock Booking mockBooking;
@InjectMocks Room room=new Room(101,RoomType.SINGLE);
@BeforeEach
void setUp() throws Exception {
	hotel=new Hotel();
	bookingCTL= new BookingCTL(hotel);
	arrivalDate=format.parse("11-12-2100");	
	bookingsByConfirmationNumber = new HashMap<>();
	activeBookingsByRoomId= new HashMap<>();
	hotel=new Hotel();
	booking =new Booking(guest,room,arrivalDate,stayLength,numberOfOccupants,card);

}
/* Added by Rajashekar Boreddy*/
/*Tested By Rajashekar Boreddy*/
	@Test
	final void testCreditDetailsEntered() {
		//arrange
		//act
		bookingCTL.phoneNumberEntered(0450750);
		bookingCTL.guestDetailsEntered("Praveen","Test");
		selectedRoomType = selectedRoomType.TWIN_SHARE;
		bookingCTL.roomTypeAndOccupantsEntered(selectedRoomType, 1);
		bookingCTL.bookingTimesEntered(arrivalDate, 2);
		bookingCTL.creditDetailsEntered(type, 125, 1);
	}
	@Test
	final void testCreditDetailsEntered_showExceptions() {
		//arrange
		//act
		Executable e = () -> bookingCTL.creditDetailsEntered(type,number,ccv);
		Throwable t = assertThrows(RuntimeException.class,e);
		//assert
		assertEquals("Error : Please check the booking!!",t.getMessage());		

	}

}
