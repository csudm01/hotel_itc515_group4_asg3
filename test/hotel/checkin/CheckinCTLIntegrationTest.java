package hotel.checkin;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import hotel.credit.CreditCard;
import hotel.credit.CreditCardType;
import hotel.entities.*;

class CheckinCTLIntegrationTest {
	Hotel hotel;
	CheckinCTL checkinCTL;
	Room room=new Room(101,RoomType.SINGLE);
	Guest guest=new Guest("Ramesh","Sydney",0435);
	Date arrivalDate;
	
	
	int stayLength=2;
	int occupantNumber=2;
	CreditCard creditCard=new CreditCard(CreditCardType.VISA,001,074);
	
	@BeforeEach
	void setUp() throws Exception {
		hotel=new Hotel();
		SimpleDateFormat format=new SimpleDateFormat("dd-MM-yyyy");
		arrivalDate=format.parse("27-07-2001");
	}

	@AfterEach
	void tearDown() throws Exception {
	}
	
	/*
	 * Author: Topman Garbuja
	 * Testing: High-level Integration testing
	 * Objective:
	 * After the checkIn is confirmed in CheckinCTL
	 * 	    -activeBookingsByRoomId of Hotel must have one entry for the corresponding booking.
	 * 		-The corresponding booking should have state of CHECKED_IN which was initially in PENDING state
	 * 		-The state of corresponding booked room should have a state of OCCUPIED 
	 * 			.i.e. isReady() function should return false which initially return true
	 */
	@Test
	void testCheckInConfirmed() {
		//arrange		
		long confirmationNumber=hotel.book(room, guest, arrivalDate, stayLength, occupantNumber, creditCard);
		checkinCTL=new CheckinCTL(hotel);
		Booking booking=hotel.findBookingByConfirmationNumber(confirmationNumber);
		assertEquals(0,hotel.activeBookingsByRoomId.size());
		assertTrue(booking.isPending());
		assertTrue(room.isReady());					
				
		//act
		checkinCTL.confirmationNumberEntered(confirmationNumber);
		checkinCTL.checkInConfirmed(true);
		
		//assert		
		assertEquals(1,hotel.activeBookingsByRoomId.size());
		assertTrue(booking.isCheckedIn());
		assertFalse(room.isReady());
	}
	
	/*
	 * Author: Topman Garbuja
	 * Testing: Not-HappyScenario testing | Failed to checked in
	 * Objective:
	 * When confirmationNumber is Entered with new random number other than the TRUE confirmation number
	 * 	    -activeBookingsByRoomId of Hotel should not contain entry for the booking.	 *
	 * 		-the booking should be NULL .i.e. no booking at all
	 */
	@Test
	void testCheckInConfirmed_Fail_CheckedIn_NotHappyScenario() {
		//arrange		
		long confirmationNumber=hotel.book(room, guest, arrivalDate, stayLength, occupantNumber, creditCard);
		checkinCTL=new CheckinCTL(hotel);				
		long newConfirmationNumber=1172018101;
		Booking booking=hotel.findBookingByConfirmationNumber(newConfirmationNumber);
				
		//act
		checkinCTL.confirmationNumberEntered(newConfirmationNumber);		
		
		//assert		
		assertEquals(0,hotel.activeBookingsByRoomId.size());
		assertTrue(booking == null);		
	}
	
	
	/*
	 * Author: Topman Garbuja
	 * Testing: HappyScenario testing | Successfully checked in
	 * Objective:
	 * When confirmationNumber is Entered with the TRUE confirmation number
	 * 	    -activeBookingsByRoomId of Hotel should contain entry for the booking.	 
	 * 		-the booking is successfully Checked in 		
	 */
	@Test
	void testCheckInConfirmed_Sucessfully_CheckedIn_HappyScenario() {
		//arrange	
		long confirmationNumber=hotel.book(room, guest, arrivalDate, stayLength, occupantNumber, creditCard);
		Booking booking=hotel.findBookingByConfirmationNumber(confirmationNumber);
		checkinCTL=new CheckinCTL(hotel);

		//act
		checkinCTL.confirmationNumberEntered(confirmationNumber);
		checkinCTL.checkInConfirmed(true);
		
		//assert		
		assertEquals(1,hotel.activeBookingsByRoomId.size());		
		assertTrue(booking.isCheckedIn());
	}	
}
