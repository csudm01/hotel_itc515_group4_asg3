package hotel.checkin;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import hotel.credit.CreditCard;
import hotel.credit.CreditCardType;
import hotel.entities.*;

@ExtendWith(MockitoExtension.class)
class CheckinCTLTest {		
	@Mock Hotel hotel;
	CheckinCTL checkinCTL;
	

	@BeforeEach
	void setUp() throws Exception {
		checkinCTL=new CheckinCTL(hotel);
	}
	
	
	/*
	 * Author: Topman Garbuja
	 * Testing: Unit testing
	 * Objective:
	 * When the checkinCTL checkinConfirmed is invoked when its state is not CONFIRMING
	 * 	    -it should throw an error
	 */
	@Test
	void testCheckInConfirmed__ShouldThrowException_IfStateIsNotConfirming() {
		//arrange
		//act
		Executable e = () -> checkinCTL.checkInConfirmed(true);
		Throwable t = assertThrows(RuntimeException.class,e);
		
		//assert
		assertEquals("The state must be in CONFIRMING state before check in is confirmed.",t.getMessage());		
	}

}
