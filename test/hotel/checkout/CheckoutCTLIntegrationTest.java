package hotel.checkout;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import hotel.checkin.*;
import hotel.checkout.*;
import hotel.credit.CreditCard;
import hotel.credit.CreditCardType;
import hotel.entities.*;

class CheckoutCTLTest {
	Hotel hotel;
	CheckoutCTL control;
	Room room=new Room(101,RoomType.SINGLE);
	Guest guest=new Guest("Sambanda","Auburn",1234);
	Date date;
	
	
	int stayLength=2;
	int numberOfOccupants=2;
	CreditCard creditCard=new CreditCard(CreditCardType.MASTERCARD,2,95);
	
	@BeforeEach
	void setUp() throws Exception {
		hotel=new Hotel();
		SimpleDateFormat format=new SimpleDateFormat("dd-MM-yyyy");
		date=format.parse("27-07-2019");		
		control=new CheckoutCTL(hotel);
	}
	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testCreditDetailsEntered() {
		long number=hotel.book(room, guest, date, stayLength,numberOfOccupants, creditCard);
		Booking booking=hotel.findBookingByConfirmationNumber(number);		
		//control.run();
		control.roomIdEntered(room.getId());
		control.chargesAccepted(true);
		control.creditDetailsEntered(CreditCardType.MASTERCARD, 2,29);
		
		assertEquals(0,hotel.activeBookingsByRoomId.size());
		assertTrue(booking.isCheckedOut());
		assertTrue(room.isReady());		
		
	}

}
