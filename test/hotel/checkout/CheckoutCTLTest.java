package hotel.checkout;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Date;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import hotel.credit.CreditCard;
import hotel.credit.CreditCardType;
import hotel.entities.Hotel;

@ExtendWith(MockitoExtension.class)
class CheckoutCTLTest {
	@Mock Hotel hotel;
	CheckoutCTL control;
	
	
	
	@BeforeEach
	void setUp() throws Exception {
		control=new CheckoutCTL(hotel);
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testCreditDetailsEntered() {
		
		Executable e = () -> control.creditDetailsEntered(CreditCardType.MASTERCARD, 1, 24);
		Throwable t = assertThrows(RuntimeException.class,e);
		assertEquals("Error : Please check the booking!!",t.getMessage());
	}


}
