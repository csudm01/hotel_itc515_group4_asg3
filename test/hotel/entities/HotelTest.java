package hotel.entities;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import hotel.checkin.CheckinCTL;
import hotel.credit.CreditCard;
import hotel.credit.CreditCardType;
import hotel.entities.*;
	
/*Added By Praveen*/
/*Fully Tested By Praveen*/

public class HotelTest {
	@Mock Hotel hotel;
	@Mock Guest guest;	
	@Mock CreditCard card;
	@Mock Booking confirmationNumber;
	SimpleDateFormat format=new SimpleDateFormat("11-12-2001");
	Date arrivalDate;
	int stayLength=2;
	ServiceType serviceType;
	int numberOfOccupants=5;
	Booking booking;
	public Map<Long, Booking> bookingsByConfirmationNumber;
	public Map<Integer, Booking> activeBookingsByRoomId;
	enum State {OCCUPIED,READY};
	List adding;
	State state;
	@Mock Booking mockBooking;
	@InjectMocks Room room=new Room(101,RoomType.SINGLE);
	@BeforeEach

	void setUp() throws Exception {
		arrivalDate=format.parse("11-12-2001");	
		bookingsByConfirmationNumber = new HashMap<>();
		activeBookingsByRoomId= new HashMap<>();
		hotel=new Hotel();
		booking =new Booking(guest,room,arrivalDate,stayLength,numberOfOccupants,card);

	}
/*Added By Praveen*/
/*Fully Tested By Praveen*/
	@Test
	public final void testBook() {
		assertEquals(booking.confirmationNumber,hotel.book(room,guest,arrivalDate,stayLength,numberOfOccupants,card));	
	}
/*Added By Praveen*/
/*Fully Tested By Praveen*/
	@Test
	public final void testCheckin() {
		hotel.book(room,guest,arrivalDate,stayLength,numberOfOccupants,card);
		hotel.checkin(101970101);
		assertEquals(false,room.isReady());
	}
/*Added By Praveen*/
/*Fully Tested By Praveen*/
	@Test
	final void testCheckIn_ShouldThrowException_IfStateIsNotCheckedIn() {
		//arrange
		//act
		Executable e = () -> hotel.checkin(12345);
		Throwable t = assertThrows(RuntimeException.class,e);
		
		//assert
		assertEquals("Error : There are No Bookings Please Verify",t.getMessage());		
	}
/*Added By Praveen*/
/*Fully Tested By Praveen*/
	@Test
	public final void testAddServiceCharge() {
		hotel.book(room,guest,arrivalDate,stayLength,numberOfOccupants,card);
		hotel.checkin(101970101);
		hotel.addServiceCharge(101, serviceType, 100.20);
		
	}
/*Added By Praveen*/
/*Fully Tested By Praveen*/
	@Test
	public final void testAddServiceCharge_ThrowsException() {
		hotel.book(room,guest,arrivalDate,stayLength,numberOfOccupants,card);
		hotel.checkin(101970101);
		Executable e = () -> hotel.addServiceCharge(501, serviceType, 0);
		Throwable t = assertThrows(RuntimeException.class,e);
		
		//assert
		assertEquals("Error : There are No Active Bookings Please Verify",t.getMessage());		

		
		
	}
/*Added By Praveen*/
/*Fully Tested By Praveen*/
	@Test
	public final void testCheckout() {
		hotel.book(room,guest,arrivalDate,stayLength,numberOfOccupants,card);
		hotel.checkin(101970101);
		hotel.checkout(101);
		assertEquals(true,room.isReady());
		
		
	}
/*Added By Praveen*/
/*Fully Tested By Praveen*/
	@Test
	final void testCheckOut_ShouldThrowException_IfStateIsNotCheckedIn() {
		//arrange
		//act
		Executable e = () -> hotel.checkout(101);
		Throwable t = assertThrows(RuntimeException.class,e);
		
		//assert
		assertEquals("Error : There are No Active Bookings to Check out Please Verify",t.getMessage());		
	}

}
