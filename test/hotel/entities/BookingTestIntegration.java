package hotel.entities;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import hotel.checkin.CheckinCTL;
import hotel.credit.CreditCard;
import hotel.credit.CreditCardType;
import hotel.entities.*;

@ExtendWith(MockitoExtension.class)
class BookingTestIntegration {	
	
	Hotel hotel;
	CheckinCTL checkinCTL;
	Room room;
	Guest guest;
	Date arrivalDate;
	SimpleDateFormat format=new SimpleDateFormat();	
	CreditCard creditCard;	
		
	Booking booking;	
	
	@BeforeEach
	void setUp() throws Exception {	
		format=new SimpleDateFormat("dd-MM-yyyy");
		arrivalDate=format.parse("11-12-2001");
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	
	/*
	 * Author: Topman Garbuja
	 * Testing: Low-level Integration testing
	 * Objective:
	 * After the booking checkIn is executed
	 * 	    -the room associated with the booking state should be OCCUPIED 
	 * 				i.e. that isReady() should return false which initially return true before checkIn()
	 * 		-the state of the booking should be CHECKED_IN
	 */
	@Test
	final void testCheckIn() {		
		//arrange		
		guest=new Guest("Ramesh","Sydney",0435);
		room=new Room(101,RoomType.SINGLE);					
		int stayLength=2;
		int numberOfOccupants=5;	
		creditCard=new CreditCard(CreditCardType.VISA,001,074);		
		booking=room.book(guest,arrivalDate,stayLength,numberOfOccupants,creditCard);
		assertTrue(room.isReady());
		assertTrue(booking.isPending());		
		
		//act		
		booking.checkIn();						
				
		//assert
		assertFalse(room.isReady());
		assertTrue(booking.isCheckedIn());				
	}	
	
	
	/*
	 * Author: Praveen Kumar Kokkerapati
	 * Testing: Unit testing
	 * Objective:
	 * When the booking addServiceCharge is invoked
	 * 	    Checks the Service Charges 
	 */
	@Test
	final void testaddServiceCharge() {
		//arrange
		//act		
		booking.addServiceCharge(serviceType, 100.20);
		adding=booking.getCharges();
		ServiceCharge Ser=(ServiceCharge) adding.get(0);
		assertEquals(Ser.getCost(),100.2);
			
	}
	/*
	 * Author: Topman Garbuja
	 * Testing: Low-level Integration testing
	 * Objective:
	 * After the booking checkOut is executed
	 * 	    -the room associated with the booking state should be in READY state 
	 * 				i.e. that isReady() should return true
	 * 		-the booking state of the booking should be CHECKED_OUT
	 * 		-the room bookings size must be back to 0 
	 */
	@Test
	final void testCheckOut() {		
		//arrange
		guest=new Guest("Ramesh","Sydney",0435);
		room=new Room(101,RoomType.SINGLE);					
		int stayLength=2;
		int numberOfOccupants=5;	
		creditCard=new CreditCard(CreditCardType.VISA,001,074);		
		booking=room.book(guest,arrivalDate,stayLength,numberOfOccupants,creditCard);		
		assertTrue(room.isReady());
		assertTrue(booking.isPending());		
		assertEquals(1,room.bookings.size());
		
		//act		
		booking.checkIn();
		booking.checkOut();
		
		//assert
		assertTrue(room.isReady());
		assertTrue(booking.isCheckedOut());
		assertEquals(0,room.bookings.size());
	}		
}
