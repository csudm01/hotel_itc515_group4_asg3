package hotel.entities;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import hotel.credit.CreditCard;

@ExtendWith(MockitoExtension.class)
class BookingTest {
	@Mock Guest guest;
	@Mock Room room;	
	@Mock CreditCard card;
	List adding;
	ServiceType serviceType;
	SimpleDateFormat format=new SimpleDateFormat();
	
	Booking booking;
	@BeforeEach
	void setUp() throws Exception {		
		format=new SimpleDateFormat("dd-MM-yyyy");
		Date arrivalDate=format.parse("11-12-2001");
		int stayLength=2;
		int numberOfOccupants=5;		
		
		booking=new Booking(guest,room,arrivalDate,stayLength,numberOfOccupants,card);
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	/*
	 * Author: Praveen Kumar Kokkerapati
	 * Testing: Unit testing
	 * Objective:
	 * When the booking addServiceCharge is invoked
	 * 	    Checks the Service Charges 
	 */
	@Test
	final void testaddServiceCharge() {
		//arrange
		//act		
		booking.addServiceCharge(serviceType, 100.20);
		adding=booking.getCharges();
		ServiceCharge Ser=(ServiceCharge) adding.get(0);
		assertEquals(Ser.getCost(),100.2);
			
	}
	/*
	 * Author: Topman Garbuja
	 * Testing: Unit testing
	 * Objective:
	 * When the booking checkIn is invoked
	 * 	    -the booking state must be in CHECKED_IN STATE
	 */
	@Test
	final void testCheckIn_BookingState_MustBeSetToCheckedIn() {
		//arrange
		//act		
		booking.checkIn();
		
		//assert
		assertTrue(booking.isCheckedIn());		
	}
	
	/*
	 * Author: Topman Garbuja
	 * Testing: Unit testing
	 * Objective:
	 * When the booking checkIn is invoked
	 * 	    -the room associated with the booking must invoke it's checkin() method one time
	 */
	@Test
	final void testCheckIn_ShouldCallRoomCheckIn_Once(){
		//arrange
		Room associatedRoom=booking.getRoom();
		
		//act
		booking.checkIn();
		
		//assert		
		verify(associatedRoom,times(1)).checkin();	
	}

	/*
	 * Author: Topman Garbuja
	 * Testing: Unit testing
	 * Objective:
	 * When the booking checkOut is invoked
	 * 	    -the booking state must be CHECKED_OUT
	 */
	@Test
	final void testCheckOut_BookingState_MustBeSetToCheckedOut() {
		//arrange
		booking.checkIn();
		
		//act
		booking.checkOut();
		
		//asserts
		assertTrue(booking.isCheckedOut());		
	}
	
	
	/*
	 * Author: Topman Garbuja
	 * Testing: Unit testing
	 * Objective:
	 * When the booking checkOut is invoked
	 * 	    -the room associated with the booking must invoke it's checkOut() method one time
	 */
	@Test
	final void testCheckOut_ShouldCallRoomCheckOut_Once(){
		//arrange
		booking.checkIn();
		Room associatedRoom=booking.getRoom();				
		
		//act
		booking.checkOut();
		
		//assert
		verify(room,times(1)).checkout(booking);	
	}
	
	/*
	 * Author: Topman Garbuja
	 * Testing: Unit testing
	 * Objective:
	 * When the booking checkOut is invoked if its state is not CHECKED_IN
	 * 	    -it should throw an error message
	 */
	@Test
	final void testCheckOut_ShouldThrowException_IfStateIsNotCheckedIn() {
		//arrange
		//act
		Executable e = () -> booking.checkOut();
		Throwable t = assertThrows(RuntimeException.class,e);
		
		//assert
		assertEquals("Cannot checked out if the state is not CHECKED_IN",t.getMessage());		
	}

}
