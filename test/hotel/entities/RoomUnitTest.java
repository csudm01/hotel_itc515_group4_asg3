package hotel.entities;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Date;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import hotel.credit.CreditCard;

@ExtendWith(MockitoExtension.class)
class RoomTest {
	@Mock Booking booking;
	@Mock Guest guest;
	@Mock CreditCard creditCard;
	@Spy ArrayList<Booking> bookings;

	Date arrivalDate=new Date(2012,12,12);
	int stayLength=2;
	int numberOfOccupants=5;	
	
	@InjectMocks Room room=new Room(101,RoomType.SINGLE);
	@BeforeEach
	void setUp() throws Exception {
		
		
	}
	

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testBook() {
		room.book(guest, arrivalDate, stayLength, numberOfOccupants, creditCard);
		
		assertEquals(1,bookings.size());
	}

	@Test
	void testCheckin() {
		//arrange
		//act
		room.checkin();
		//assert
		assertFalse(room.isReady());
	}
	
	@Test
	void testCheckOutException() {
		Executable e = () -> room.checkout(booking);
		Throwable t = assertThrows(RuntimeException.class,e);
		assertEquals("Cannot ckeck in to a room that is not OCCUPIED",t.getMessage());
	}

	@Test
	void testCheckOut_DoesnotContainBooking() {
		room.checkin();
		Executable e = () -> room.checkout(booking);
		Throwable t = assertThrows(RuntimeException.class,e);
		assertEquals("Cannot ckeck in to a room that is not related to the booking",t.getMessage());
	}
	
	
}
