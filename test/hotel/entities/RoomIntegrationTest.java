package hotel.entities;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Date;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import hotel.credit.CreditCard;
import hotel.credit.CreditCardType;

@ExtendWith(MockitoExtension.class)
class RoomIntegrationTest {
	Booking booking;
	Guest guest=new Guest("Susmita","Auburn",12345);
	CreditCard creditCard=new CreditCard(CreditCardType.MASTERCARD,1,309);
	Date arrivalDate=new Date(2012,12,12);
	int stayLength=2;
	int numberOfOccupants=5;	
	@Spy ArrayList<Booking> bookings;
	@InjectMocks Room room=new Room(101,RoomType.SINGLE);
	@BeforeEach
	void setUp() throws Exception {
		
		//booking=new Booking(guest,room,arrivalDate,stayLength,numberOfOccupants,creditCard);
	}
	
	/*
	 * Author: Roshan Paudel
	 * Testing: Unit testing
	 * Objective:
	 * When the test methods are implemented, booking should 
	 * be added to list and removed from the list respectively
	 */
	@AfterEach
	void tearDown() throws Exception {
	}
	/**
	 * -The book() method should return a booking object
	 * -the bookings list must contain that booking
	 * -the room must be OCCUPIED
	 */
	@Test
	void testCheckOut_ShouldAddBookingToList() {
		Booking booking=room.book(guest, arrivalDate, stayLength, numberOfOccupants, creditCard);
		
		room.checkout(booking);
		assertTrue(booking !=  null);
		assertEquals(1,bookings.size());
		assertFalse(room.isReady());
		
	}
	
	
	/**	 
	 * -the checkout method should remove booking from the list
	 * -then must be available once again
	 */
	@Test
	void testCheckOut_RemoveBookingFromList() {
		Booking booking=room.book(guest, arrivalDate, stayLength, numberOfOccupants, creditCard);
		
		room.checkout(booking);
		assertEquals(0,bookings.size());
		assertTrue(room.isReady());
	}

	

}
